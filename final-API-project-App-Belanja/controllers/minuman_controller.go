package controllers

import (
	"net/http"
	"time"

	"final-project/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type minumanInput struct {
		IdMinuman 			uint    	    `json:"id_minuman"`
		NamaBarang  		string    	    `json:"nama_barang"`
		Deskripsi  			string      	`json:"deskripsi"`
		Harga				uint	        `json:"harga"`
}

// GetAllMinuman godoc
// @Summary Get all Minuman.
// @Description Get a list of Minuman.
// @Tags Minuman
// @Produce json
// @Success 200 {object} []models.Minuman
// @Router /minuman [get]
func GetAllMinuman(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var minuman []models.Minuman
    db.Find(&minuman)

    c.JSON(http.StatusOK, gin.H{"data": minuman})
}

// CreateMinuman godoc
// @Summary Create New Minuman.
// @Description Creating a new Minuman.
// @Tags Minuman
// @Param Body body minumanInput true "the body to create a new Minuman"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Minuman
// @Router /minuman [post]
func CreateMinuman(c *gin.Context) {
    // Validate input
    var input minumanInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }
    // Create Minuman
    minuman := models.Minuman{
		IdMinuman: input.IdMinuman, NamaBarang: input.NamaBarang, 
		Deskripsi: input.Deskripsi, Harga: input.Harga,
	}
    db := c.MustGet("db").(*gorm.DB)
    db.Create(&minuman)

    c.JSON(http.StatusOK, gin.H{"data": minuman})
}

// GetMinumanById godoc
// @Summary Get Minuman.
// @Description Get an Minuman by id.
// @Tags Minuman
// @Produce json
// @Param id path string true "Minuman id"
// @Success 200 {object} models.Minuman
// @Router /minuman/{id} [get]
func GetMinumanById(c *gin.Context) { 
    var minuman models.Minuman

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&minuman).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": minuman})
}

// UpdateMinuman godoc
// @Summary Update Minuman.
// @Description Update Minuman by id.
// @Tags Minuman
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "Minuman id"
// @Param Body body minumanInput true "the body to update minuman"
// @Success 200 {object} models.Minuman
// @Router /minuman/{id} [patch]
func UpdateMinuman(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    var minuman models.Makanan
    if err := db.Where("id = ?", c.Param("id")).First(&minuman).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }
    // Validate input
    var input minumanInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }
    var updatedInput models.Makanan
    updatedInput.NamaBarang = input.NamaBarang
    updatedInput.Deskripsi = input.Deskripsi
	updatedInput.Harga = input.Harga
    updatedInput.UpdatedAt = time.Now()

    db.Model(&minuman).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": minuman})
}

// DeleteMinuman godoc
// @Summary Delete one Minuman.
// @Description Delete a Minuman by id.
// @Tags Minuman
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "Minuman id"
// @Success 200 {object} map[string]boolean
// @Router /minuman/{id} [delete]
func DeleteMinuman(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)
    var minuman models.Makanan
    if err := db.Where("id = ?", c.Param("id")).First(&minuman).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    db.Delete(&minuman)

    c.JSON(http.StatusOK, gin.H{"data": true})
}