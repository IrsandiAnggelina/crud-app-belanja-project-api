package controllers

import (
	"net/http"
	"time"

	"final-project/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type elektronikInput struct {
		IdElektronik 			uint    	 `json:"id_elektronik"`
		NamaBarang  		string    	    `json:"nama_barang"`
		Deskripsi  			string      	`json:"deskripsi"`
		Harga				uint	        `json:"harga"`
}

// GetAllElektronik godoc
// @Summary Get all Elektronik.
// @Description Get a list of Elektronik.
// @Tags Elektronik
// @Produce json
// @Success 200 {object} []models.Elektronik
// @Router /elektronik [get]
func GetAllElektronik(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var elektronik []models.Elektronik
    db.Find(&elektronik)

    c.JSON(http.StatusOK, gin.H{"data": elektronik})
}

// CreateElektronik godoc
// @Summary Create New Elektronik.
// @Description Creating a new Elektronik.
// @Tags Elektronik
// @Param Body body elektronikInput true "the body to create a new Elektronik"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Elektronik
// @Router /elektronik [post]
func CreateElektronik(c *gin.Context) {
    // Validate input
    var input elektronikInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }
    // Create Makanan
    elektronik := models.Minuman{
		IdMinuman: input.IdElektronik, NamaBarang: input.NamaBarang, 
		Deskripsi: input.Deskripsi, Harga: input.Harga,
	}
    db := c.MustGet("db").(*gorm.DB)
    db.Create(&elektronik)

    c.JSON(http.StatusOK, gin.H{"data": elektronik})
}

// GetElektronikById godoc
// @Summary Get Elektronik.
// @Description Get an Elektronik by id.
// @Tags Elektronik
// @Produce json
// @Param id path string true "Elektronik id"
// @Success 200 {object} models.Elektronik
// @Router /elektronik/{id} [get]
func GetElektronikById(c *gin.Context) { 
    var elektronik models.Elektronik

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&elektronik).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": elektronik})
}

// UpdateElektronik godoc
// @Summary Update Elektronik.
// @Description Update Elektronik by id.
// @Tags Elektronik
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "Elektronik id"
// @Param Body body elektronikInput true "the body to update elektronik"
// @Success 200 {object} models.Elektronik
// @Router /elektronik/{id} [patch]
func UpdateElektronik(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    var elektronik models.Elektronik
    if err := db.Where("id = ?", c.Param("id")).First(&elektronik).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }
    // Validate input
    var input elektronikInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }
    var updatedInput models.Elektronik
    updatedInput.NamaBarang = input.NamaBarang
    updatedInput.Deskripsi = input.Deskripsi
	updatedInput.Harga = input.Harga
    updatedInput.UpdatedAt = time.Now()

    db.Model(&elektronik).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": elektronik})
}

// DeleteElektronik godoc
// @Summary Delete one Elektronik.
// @Description Delete a Elektronik by id.
// @Tags Elektronik
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "Elektronik id"
// @Success 200 {object} map[string]boolean
// @Router /elektronik/{id} [delete]
func DeleteElektronik(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)
    var elektronik models.Elektronik
    if err := db.Where("id = ?", c.Param("id")).First(&elektronik).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    db.Delete(&elektronik)

    c.JSON(http.StatusOK, gin.H{"data": true})
}