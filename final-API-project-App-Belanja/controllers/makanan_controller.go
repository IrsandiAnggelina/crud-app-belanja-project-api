package controllers

import (
	"net/http"
	"time"

	"final-project/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type makananInput struct {
		IdMakanan  			uint    	    `json:"id_makanan"`
		NamaBarang  		string    	    `json:"nama_barang"`
		Deskripsi  			string      	`json:"deskripsi"`
		Harga				uint	        `json:"harga"`
}

// GetAllMakanan godoc
// @Summary Get all Makanan.
// @Description Get a list of Makanan.
// @Tags Makanan
// @Produce json
// @Success 200 {object} []models.Makanan
// @Router /makanan [get]
func GetAllMakanan(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var makanan []models.Makanan
    db.Find(&makanan)

    c.JSON(http.StatusOK, gin.H{"data": makanan})
}

// CreateMakanan godoc
// @Summary Create New Makanan.
// @Description Creating a new Makanan.
// @Tags Makanan
// @Param Body body makananInput true "the body to create a new Makanan"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Makanan
// @Router /makanan [post]
func CreateMakanan(c *gin.Context) {
    // Validate input
    var input makananInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }
    // Create Makanan
    makanan := models.Makanan{
		IdMakanan: input.IdMakanan, NamaBarang: input.NamaBarang, 
		Deskripsi: input.Deskripsi, Harga: input.Harga,
	}
    db := c.MustGet("db").(*gorm.DB)
    db.Create(&makanan)

    c.JSON(http.StatusOK, gin.H{"data": makanan})
}

// GetMakananById godoc
// @Summary Get Makanan.
// @Description Get an Makanan by id.
// @Tags Makanan
// @Produce json
// @Param id path string true "Makanan id"
// @Success 200 {object} models.Makanan
// @Router /makanan/{id} [get]
func GetMakananById(c *gin.Context) { 
    var makanan models.Makanan

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&makanan).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": makanan})
}

// UpdateMakanan godoc
// @Summary Update Makanan.
// @Description Update Makanan by id.
// @Tags Makanan
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "Makanan id"
// @Param Body body makananInput true "the body to update makanan"
// @Success 200 {object} models.Makanan
// @Router /makanan/{id} [patch]
func UpdateMakanan(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    var makanan models.Makanan
    if err := db.Where("id = ?", c.Param("id")).First(&makanan).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }
    // Validate input
    var input makananInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }
    var updatedInput models.Makanan
    updatedInput.NamaBarang = input.NamaBarang
    updatedInput.Deskripsi = input.Deskripsi
	updatedInput.Harga = input.Harga
    updatedInput.UpdatedAt = time.Now()

    db.Model(&makanan).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": makanan})
}

// DeleteMakanan godoc
// @Summary Delete one Makanan.
// @Description Delete a Makanan by id.
// @Tags Makanan
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "Makanan id"
// @Success 200 {object} map[string]boolean
// @Router /makanan/{id} [delete]
func DeleteMakanan(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)
    var makanan models.Makanan
    if err := db.Where("id = ?", c.Param("id")).First(&makanan).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    db.Delete(&makanan)

    c.JSON(http.StatusOK, gin.H{"data": true})
}