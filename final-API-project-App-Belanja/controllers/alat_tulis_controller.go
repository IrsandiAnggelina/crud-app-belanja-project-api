package controllers

import (
	"net/http"
	"time"

	"final-project/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type alatTulisInput struct {
		IdAlatTulis 			uint    	 `json:"id_alat_tulis"`
		NamaBarang  		string    	    `json:"nama_barang"`
		Deskripsi  			string      	`json:"deskripsi"`
		Harga				uint	        `json:"harga"`
}

// GetAllAlatTulis godoc
// @Summary Get all AlatTulis.
// @Description Get a list of AlatTulis.
// @Tags AlatTulis
// @Produce json
// @Success 200 {object} []models.AlatTulis
// @Router /alat-tulis [get]
func GetAllAlatTulis(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var alat_tulis []models.AlatTulis
    db.Find(&alat_tulis)

    c.JSON(http.StatusOK, gin.H{"data": alat_tulis})
}

// CreateAlatTulis godoc
// @Summary Create New AlatTulis.
// @Description Creating a new AlatTulis.
// @Tags AlatTulis
// @Param Body body alatTulisInput true "the body to create a new AlatTulis"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.AlatTulis
// @Router /alat-tulis [post]
func CreateAlatTulis(c *gin.Context) {
    // Validate input
    var input alatTulisInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

     // Create AlatTulis
    alat_tulis := models.AlatTulis{
		IdAlatTulis: input.IdAlatTulis, NamaBarang: input.NamaBarang, 
		Deskripsi: input.Deskripsi, Harga: input.Harga,
	}
    db := c.MustGet("db").(*gorm.DB)
    db.Create(&alat_tulis)

    c.JSON(http.StatusOK, gin.H{"data": alat_tulis})
}

// GetAlatTulisById godoc
// @Summary Get AlatTulis.
// @Description Get an AlatTulis by id.
// @Tags AlatTulis
// @Produce json
// @Param id path string true "AlatTulis id"
// @Success 200 {object} models.AlatTulis
// @Router /alat-tulis/{id} [get]
func GetAlatTulisById(c *gin.Context) { 
    var alat_tulis models.AlatTulis

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&alat_tulis).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": alat_tulis})
}

// UpdateAlatTulis godoc
// @Summary Update AlatTulis.
// @Description Update AlatTulis by id.
// @Tags AlatTulis
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "AlatTulis id"
// @Param Body body alatTulisInput true "the body to update alat tulis"
// @Success 200 {object} models.AlatTulis
// @Router /alat-tulis/{id} [patch]
func UpdateAlatTulis(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    var alat_tulis models.AlatTulis
    if err := db.Where("id = ?", c.Param("id")).First(&alat_tulis).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }
    // Validate input
    var input alatTulisInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }
    var updatedInput models.AlatTulis
    updatedInput.NamaBarang = input.NamaBarang
    updatedInput.Deskripsi = input.Deskripsi
	updatedInput.Harga = input.Harga
    updatedInput.UpdatedAt = time.Now()

    db.Model(&alat_tulis).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": alat_tulis})
}

// DeleteAlatTulis godoc
// @Summary Delete one AlatTulis.
// @Description Delete a AlatTulis by id.
// @Tags AlatTulis
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "AlatTulis id"
// @Success 200 {object} map[string]boolean
// @Router /alat-tulis/{id} [delete]
func DeleteAlatTulis(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)
    var alat_tulis models.AlatTulis
    if err := db.Where("id = ?", c.Param("id")).First(&alat_tulis).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    db.Delete(&alat_tulis)

    c.JSON(http.StatusOK, gin.H{"data": true})
}