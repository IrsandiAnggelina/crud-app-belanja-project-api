package controllers

import (
	"final-project/models"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type kategoriProdukInput struct {
	NamaKategori  	string          `json:"nama_kategori"`
	IdKategori		uint		    `json:"id_kategori"`
}

// GetAllCategory godoc
// @Summary Get all KategoriProduk.
// @Description Get a list of KategoriProduk.
// @Tags KategoriProduk
// @Produce json
// @Success 200 {object} []models.KategoriProduk
// @Router /kategori-produk [get]
func GetAllCategory(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var kategori []models.KategoriProduk
    db.Find(&kategori)

    c.JSON(http.StatusOK, gin.H{"data": kategori})
}

// GetCategoryById godoc
// @Summary Get KategoriProduk.
// @Description Get an KategoriProduk by id.
// @Tags KategoriProduk
// @Produce json
// @Param id path string true "KategoriProduk id"
// @Success 200 {object} models.KategoriProduk
// @Router /kategori-produk/{id} [get]
func GetCategoryById(c *gin.Context) { 
    var kategori models.KategoriProduk

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&kategori).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": kategori})
}
