package models

import (
	"time"
)

type (
    AlatTulis struct {
        ID          		uint     	    `gorm:"primary_key" json:"id"`
		IdAlatTulis 			uint    	`json:"id_alat_tulis"`
		NamaBarang  		string    	    `json:"nama_barang"`
		Deskripsi  			string    	    `json:"deskripsi"`
		Harga				uint		    `json:"harga"`
        CreatedAt  		 	time.Time 	    `json:"created_at"`
        UpdatedAt   		time.Time   	`json:"updated_at"`
        ProdukCategory      KategoriProduk  `json:"-"`
    }
)