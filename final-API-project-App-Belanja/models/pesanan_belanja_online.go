package models

import (
	"time"
)

type (
    //Pesanan Belanja Online
    Pesanan struct {
        ID          		uint     	     `gorm:"primary_key" json:"id"`
        IdKategori  	    uint    	     `json:"id_kategori"`
		NamaBarang  		string    	     `json:"nama_barang"`
		Jumlah  			uint    	     `json:"jumlah"`
		Harga  				uint   		     `json:"harga"`
		TotalBayar			uint		     `json:"total_bayar"`
        CreatedAt  		 	time.Time   	 `json:"created_at"`
        UpdatedAt   		time.Time   	 `json:"updated_at"`
        ProdukCategory      []KategoriProduk   `json:"-"`
        Pelanggan           Pelanggan                    `json:"-"`
    }
)