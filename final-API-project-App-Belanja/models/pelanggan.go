package models

import (
	"time"
)

type (
    Pelanggan struct {
        ID          uint      `gorm:"primary_key" json:"id"`
        Name        string    `json:"name"`
        Phone		uint    `json:"phone"`
		Email		string	  `json:"email"`
        CreatedAt   time.Time `json:"created_at"`
        UpdatedAt   time.Time `json:"updated_at"`
        Pesanan     []Pesanan   `json:"-"`
    }
)