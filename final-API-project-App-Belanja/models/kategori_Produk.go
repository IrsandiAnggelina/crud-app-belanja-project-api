package models

import (
	"time"
)

type (
    //Daftar kategori Produk Belanjaan
    KategoriProduk struct {
        ID          	uint            `gorm:"primary_key" json:"id"`
        NamaKategori  	string          `json:"nama_kategori"`
		IdKategori		uint		    `json:"id_kategori"`
        CreatedAt  		time.Time       `json:"created_at"`
        UpdatedAt   	time.Time       `json:"updated_at"`
        Elektronik      []Elektronik    `json:"-"`
        Makanan         []Makanan       `json:"-"`
        Minuman         []Minuman       `json:"-"`
        AlatTulis       []AlatTulis      `json:"-"`
        Pesanan         Pesanan   `json:"-"`
    }
)