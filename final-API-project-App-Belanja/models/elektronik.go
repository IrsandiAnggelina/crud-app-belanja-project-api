package models

import (
	"time"
)

type (
    Elektronik struct {
        ID          		uint     	    `gorm:"primary_key" json:"id"`
		IdElektronik  		uint    	     `json:"id_elektronik"`
		NamaBarang  		string    	    `json:"nama_barang"`
		Deskripsi  			string      	`json:"deskripsi"`
		Harga				uint		    `json:"harga"`
        CreatedAt  		 	time.Time 	    `json:"created_at"`
        UpdatedAt   		time.Time 	    `json:"updated_at"`
        ProdukCategory      KategoriProduk  `json:"-"`
    }
)