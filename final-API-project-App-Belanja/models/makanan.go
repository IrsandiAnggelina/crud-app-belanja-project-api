package models

import (
	"time"
)

type (
    Makanan struct {
        ID          		uint     	    `gorm:"primary_key" json:"id"`
		IdMakanan  			uint    	    `json:"id_makanan"`
		NamaBarang  		string    	    `json:"nama_barang"`
		Deskripsi  			string      	`json:"deskripsi"`
		Harga				uint	        `json:"harga"`
        CreatedAt  		 	time.Time 	    `json:"created_at"`
        UpdatedAt   		time.Time 	    `json:"updated_at"`
        ProdukCategory      KategoriProduk  `json:"-"`
    }
)