package models

import (
	"time"
)

type (
    Minuman struct {
        ID          		uint     	    `gorm:"primary_key" json:"id"`
		IdMinuman 			uint    	    `json:"id_minuman"`
		NamaBarang  		string    	    `json:"nama_barang"`
		Deskripsi  			string      	`json:"deskripsi"`
		Harga				uint		    `json:"harga"`
        CreatedAt  		 	time.Time    	`json:"created_at"`
        UpdatedAt   		time.Time   	`json:"updated_at"`
        ProdukCategory      KategoriProduk  `json:"-"`
    }
)