package routes

import (
	"final-project/controllers"
	"final-project/middlewares"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	swaggerFiles "github.com/swaggo/files"     // swagger embed files
	ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware
)

func SetupRouter(db *gorm.DB) *gin.Engine {
    r := gin.Default()

    // set db to gin context
    r.Use(func(c *gin.Context) {
        c.Set("db", db)
    })

    //Auth Routes
    r.POST("/register", controllers.Register)
    r.POST("/login", controllers.Login)

	//Pelanggan
    r.GET("/pelanggan", controllers.GetAllPelanggan)
    pelangganMiddlewareRoute := r.Group("/pelanggan")
    pelangganMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
    pelangganMiddlewareRoute.POST("/", controllers.CreatePelanggan)
   
	//Kategori Produk
    r.GET("/produk-categories", controllers.GetAllCategory)
    r.GET("/produk-categories/:id", controllers.GetCategoryById)

	//Makanan
	r.GET("/makanan", controllers.GetAllMakanan)
    r.GET("/makanan/:id", controllers.GetMakananById)
    makananMiddlewareRoute := r.Group("/makanan")
    makananMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
    makananMiddlewareRoute.POST("/", controllers.CreateMakanan)
    makananMiddlewareRoute.PATCH("/:id", controllers.UpdateMakanan)
    makananMiddlewareRoute.DELETE("/:id", controllers.DeleteMakanan)

	//Minuman
	r.GET("/minuman", controllers.GetAllMinuman)
    r.GET("/minuman/:id", controllers.GetMinumanById)
    minumanMiddlewareRoute := r.Group("/minuman")
    minumanMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
    minumanMiddlewareRoute.POST("/", controllers.CreateMinuman)
    minumanMiddlewareRoute.PATCH("/:id", controllers.UpdateMinuman)
    minumanMiddlewareRoute.DELETE("/:id", controllers.DeleteMinuman)

	//Elektronik
	r.GET("/elektronik", controllers.GetAllElektronik)
    r.GET("/elektronik/:id", controllers.GetElektronikById)
    elektronikMiddlewareRoute := r.Group("/elektronik")
    elektronikMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
    elektronikMiddlewareRoute.POST("/", controllers.CreateElektronik)
    elektronikMiddlewareRoute.PATCH("/:id", controllers.UpdateElektronik)
    elektronikMiddlewareRoute.DELETE("/:id", controllers.DeleteElektronik)

	//Alat Tulis
	r.GET("/alat_tulis", controllers.GetAllAlatTulis)
    r.GET("/alat_tulis/:id", controllers.GetAlatTulisById)
    alatTulisMiddlewareRoute := r.Group("/alat-tulis")
    alatTulisMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
    alatTulisMiddlewareRoute.POST("/", controllers.CreateAlatTulis)
    alatTulisMiddlewareRoute.PATCH("/:id", controllers.UpdateAlatTulis)
    alatTulisMiddlewareRoute.DELETE("/:id", controllers.DeleteAlatTulis)

    //Pesanan
    r.GET("/pesanan", controllers.GetAllPesanan)
    r.GET("/pesanan/:id", controllers.GetPesananById)
    pesananMiddlewareRoute := r.Group("/pesanan")
    pesananMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
    pesananMiddlewareRoute.PATCH("/:id", controllers.UpdatePesanan)
    pesananMiddlewareRoute.DELETE("/:id", controllers.DeletePesanan)

    r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

    return r
}